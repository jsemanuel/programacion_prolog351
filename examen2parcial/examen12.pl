wx(a,[]).
medicamentos1x(neumonia,[ibuprofeno,azotromicina,claritromicina,amoxicilina,destrometorfano]).
medicamentos1x(tuberculosis,[ibuprofeno,conplejoB,pirazinamida,ribavirina,destrometorfano]).
medicamentos1x(hepatitisb,[ibuprofeno,ondansetron,omeprazol,timosina,entecavir]).
medicamentos1x(gastritis,[almax,ondansetron,emeprazol,ribavirina,ridocaina]).
medicamentos1x(sinusitis,[ibuprofeno,ampicilina,clindamicina,amoxicilina,dextrometorfano]).

sintoma1x(fiebre,neumonia).
sintoma1x(dolor_de_cabeza,neumonia).
sintoma1x(dolor_toraxico,neumonia).
sintoma1x(escalosfrios,neumonia).
sintoma1x(tos,neumonia).

sintoma1x(cansancio,tuberculosis).
sintoma1x(fiebre,tuberculosis).
sintoma1x(perdida_de_peso,tuberculosis).
sintoma1x(falta_de_apetito,tuberculosis).
sintoma1x(tos,tuberculosis).
sintoma1x(sudoracion,tuberculosis).

sintoma1x(fiebre,hepatitisb).
sintoma1x(nauseas,hepatitisb).
sintoma1x(dolor_parte_alta_de_abdomen,hepatitisb).
sintoma1x(ictericia,hepatitisb).
sintoma1x(orina_color_oscuro,hepatitisb).

sintoma1x(distencion_abdominal,gastritis).
sintoma1x(nauseas,gastritis).
sintoma1x(dolor_parte_alta_de_abdomen,gastritis).
sintoma1x(falta_de_apetito,gastritis).
sintoma1x(acidez_estomacal,gastritis).

sintoma1x(fiebre,sinusitis).
sintoma1x(dolor_de_cabeza,sinusitis).
sintoma1x(congestion_secrecion_nasal,sinusitis).
sintoma1x(dolor_de_oidos,sinusitis).
sintoma1x(tos,sinusitis).

enfermedadx1x(neumonia,20,[fiebre,dolor_toraxico,dolorcabeza,escalosfrios,tos]).
enfermedadx1x(tuberculosis,16.66,[fiebre,cansancio,perdidapeso,faltadeapetito,tos,sudoracion]).
enfermedadx1x(hepatitisb,20,[fiebre,nauseas,dolorpartealtaabdomen,ictericia,orinacoloroscuro]).
enfermedadx1x(gastritis,20,[distencionabdominal,nauseas,dolorparteabdomen,faltaapetito,acidezestomacal]).
enfermedadx1x(sinusitis,20,[fiebre,congestionsecrecionnasal,dolorcabeza,doloroidos,tos]).

exa(L):-
	diagnostic(L,LS),
	append(LS,L,M),
	nl,write(LS),write(' '),write(M).


diagnostic(L,LS):-
	wx(W,L1),
	enfermedadx1x(E,R,Es),
	findall(X,sintoma1x(X,E),M),
%	nl,write('ah1n1'),write(' '),write(M),
	subset(L,M),
	length(L,T),
	P is (T*R),
	nl,write(E),write(' '),write(P),write('%'),
	append([(E,P)],L1,LS),
	nl,write(L1),write(' '),write(LS).







medicamentos1(neumonia,[ibuprofeno,azotromicina,claritromicina,amoxicilina,destrometorfano]).
medicamentos1(tuberculosis,[ibuprofeno,conplejoB,pirazinamida,ribavirina,destrometorfano]).
medicamentos1(hepatitisb,[ibuprofeno,ondansetron,omeprazol,timosina,entecavir]).
medicamentos1(gastritis,[almax,ondansetron,emeprazol,ribavirina,ridocaina]).
medicamentos1(sinusitis,[ibuprofeno,ampicilina,clindamicina,amoxicilina,dextrometorfano]).

sintoma1(fiebre,neumonia).
sintoma1(dolor_de_cabeza,neumonia).
sintoma1(dolor_toraxico,neumonia).
sintoma1(escalosfrios,neumonia).
sintoma1(tos,neumonia).

sintoma1(cansancio,tuberculosis).
sintoma1(fiebre,tuberculosis).
sintoma1(perdida_de_peso,tuberculosis).
sintoma1(falta_de_apetito,tuberculosis).
sintoma1(tos,tuberculosis).
sintoma1(sudoracion,tuberculosis).

sintoma1(fiebre,hepatitisb).
sintoma1(nauseas,hepatitisb).
sintoma1(dolor_parte_alta_de_abdomen,hepatitisb).
sintoma1(ictericia,hepatitisb).
sintoma1(orina_color_oscuro,hepatitisb).

sintoma1(distencion_abdominal,gastritis).
sintoma1(nauseas,gastritis).
sintoma1(dolor_parte_alta_de_abdomen,gastritis).
sintoma1(falta_de_apetito,gastritis).
sintoma1(acidez_estomacal,gastritis).

sintoma1(fiebre,sinusitis).
sintoma1(dolor_de_cabeza,sinusitis).
sintoma1(congestion_secrecion_nasal,sinusitis).
sintoma1(dolor_de_oidos,sinusitis).
sintoma1(tos,sinusitis).

enfermedadx1(neumonia,20,[fiebre,dolor_toraxico,dolorcabeza,escalosfrios,tos]).
enfermedadx1(tuberculosis,16.66,[fiebre,cansancio,perdidapeso,faltadeapetito,tos,sudoracion]).
enfermedadx1(hepatitisb,20,[fiebre,nauseas,dolorpartealtaabdomen,ictericia,orinacoloroscuro]).
enfermedadx1(gastritis,20,[distencionabdominal,nauseas,dolorparteabdomen,faltaapetito,acidezestomacal]).
enfermedadx1(sinusitis,20,[fiebre,congestionsecrecionnasal,dolorcabeza,doloroidos,tos]).

diagnostico(L,LS):-
	w(W,L1),
	findall(X,sintoma1(X,'neumonia'),M),
%	nl,write('ah1n1'),write(' '),write(M),
	subset(L,M),
	length(L,T),
	P is (T*14.28),
	nl,write('neumonia'),write(' '),write(P),write('%'),
	append([('neumonia',P)],L1,L2),
	nl,write(L1),write(' '),write(L2),

	findall(X,sintoma1(X,'tuberculosis'),Mt),
%	nl,write('dengue'),write(' '),write(Md),
	subset(L,Mt),
	length(L,Tt),
	Pt is (Tt*14.28),
	nl,write('tuberculosis'),write(' '),write(Pt),write('%'),
%		nl,write(L2),write(' '),write(L1),
	append(L2,[('tuberculosis',Pt)],L3),

	nl,write(L3),

	findall(X,sintoma1(X,'hepatitisb'),Mh),
%	nl,write('dengue'),write(' '),write(Md),
	subset(L,Mh),
	length(L,Th),
	Ph is (Th*14.28),
	nl,write('hepatitisb'),write(' '),write(Ph),write('%'),
%		nl,write(L2),write(' '),write(L1),
	append(L3,[('hepatitisb',Ph)],L4),

	findall(X,sintoma1(X,'gastritis'),Mg),
%	nl,write('dengue'),write(' '),write(Md),
	subset(L,Mg),
	length(L,Tg),
	Pg is (Tg*14.28),
	nl,write('gastritis'),write(' '),write(Pg),write('%'),
%		nl,write(L2),write(' '),write(L1),
	append(L4,[('gastritis',Pg)],L5),

	findall(X,sintoma1(X,'sinusitis'),Ms),
%	nl,write('dengue'),write(' '),write(Md),
	subset(L,Ms),
	length(L,Ts),
	Ps is (Ts*14.28),
	nl,write('sinusitis'),write(' '),write(Ps),write('%'),
%		nl,write(L2),write(' '),write(L1),
	append(L5,[('sinusitis',Pg)],L6),
	nl,write(LS).















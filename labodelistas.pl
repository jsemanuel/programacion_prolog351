deter([],'es una lista vacia').
deter([X|_],'es una lista').
concatena(L,M,N):-
	append(L,M,N).
invierte([],[]).
invierte([X|Y],M):-
	invierte(Y,M1),
	append(M1,[X],M).
longitud([],0).
longitud([X|Y],LO):-
	longitud(Y,LO1),
	LO is LO1+1.
suma_elem([],0).
suma_elem([X|Y],S):-
	suma_elem(Y,S1),
	S is S1+X.
n_esimo(L,0):-
	write(' Esta fuera de la longitud de la Lista '),
	write(L).

n_esimo(L,N):-
	longitud(L,T),
	N>T,
	write(' Esta fuera de la longitud de la Lista '),
	write(L).

n_esimo(L,N):-
	invierte(L,L2),
	extraer(L2,N,C).
extraer([],N,0).
extraer([X|Y],N,C):-
	extraer(Y,N,C1),
	C is C1+1,
	compara(C,N,X).
compara(C,N,X):-
	C\=N.
compara(C,N,X):-
	C=N,
	write('EL ELEMENTO ES '),
	write(X).
extraer_x([],N,0).
extraer_x([X|Y],N,C):-
	extraer_x(Y,N,C1),
	C is C1+1,
	compara_x(C,N,X).
compara_x(C,N,X):-
	X\=N.
compara_x(C,N,X):-
	X=N,
	write('LA POSICION ES '),
	write(C),
	C=<1.
ultimo([],[]).
ultimo([X|Y],R):-
	ultimo(Y,R),
	write(X),
	break.
ultimo1(L):-
	longitud(L,T),
	invierte(L,L1),
	extraer(L1,T,C).

pos_elemento_x(L,X):-
	longitud(L,T),
	invierte(L,L1),
	extraer_x(L1,X,C).
cuenta(L,T):-
	longitud(L,T).


eje11(L,N):-
	cuenta1(L,N,1).
cuenta1([X|Y],N,C):-
	C<N,
	%write(X),
	C1 is C+1,
	cuenta1(Y,N,C1).
cuenta1([X|Y],N,C):-
	C=N,
	%write(X),
	write(Y).
eje12(L,N):-
	cuenta2(L,N,1).
cuenta2([X|Y],N,C):-
	C<N,
	write(X),
	C1 is C+1,
	cuenta2(Y,N,C1).
cuenta2([X|Y],N,C):-
	C=N,
	write(X).
	%write(Y).
eje13(L,K,N):-
	cuenta3(L,K,N,1).
cuenta3([X|Y],K,N,C):-
	C<K,
	C1 is C+1,
	cuenta3(Y,K,N,C1).
cuenta3([X|Y],K,N,C):-
	C>=K,
	C<N,
	write(X),
	C1 is C+1,
	cuenta3(Y,K,N,C1).
cuenta3([X|Y],K,N,C):-
	C>=K,
	C=N,
	write(X).
	%write(Y).
eje14(L,M):-
	longitud(L,T),
	longitud(M,U),
	T\=U,
	nl,write('LAS LISTAS SON DIFERENTES').
eje14(L,M):-
	longitud(L,T),
	longitud(M,U),
	T=U,
	R is 0,
	elem_igu(L,M,R),
	R=0,
	nl,write('LAS LISTAS SON IGUALES').
eje14(L,M):-
	longitud(L,T),
	longitud(M,U),
	T=U,
	R=1,
	nl,write('LAS LISTAS SON DIFERENTES').
elem_igu([],[],0).
elem_igu([X|Y],[A|B],R):-
	elem_igu(Y,B,R),
	X=A.
elem_igu([X|Y],[A|B],R):-
	elem_igu(Y,B,R),
	X\=A,
	R is 1.

eje15(L,M):-
	longitud(L,T),
	longitud(M,U),
	T>U,
	nl,write('LA PRIMERA LISTA NO ES SUB LISTA DE LA SEGUNDA').
eje15(L,M):-
	longitud(L,T),
	longitud(M,U),
	mientras(L,M,C).
mientras([],[],0).
mientras([],[_|_],C):-
	C=1,
	nl,write('La primera lista es sublista de la segunda').
mientras([X|Y],[A|B],C):-
%	nl,write(X),nl,write(A),
	X\=A,
	mientras([X|Y],B,C).
mientras([X|Y],[A|B],C):-
	X=A,
	mientras(Y,B,C),
	C is 1.

eje16(L,M):-
	longitud(L,T),
	longitud(M,U),
	mientras1(M,L,C).
mientras1([],[],0).
mientras1([],[_|_],C):-
	C=1,
	nl,write('La segunda lista es sublista de la segunda').
mientras1([X|Y],[A|B],C):-
%	nl,write(X),nl,write(A),
	X\=A,
	mientras1([X|Y],B,C).
mientras1([X|Y],[A|B],C):-
	X=A,
	mientras1(Y,B,C),
	C is 1.
eje17(L,M,R):-
	longitud(L,T),
	longitud(M,U),
	mientras2(L,M,C,R).
mientras2([],[],0,[]).
%mientras2([],[_|_],C,R):-
	%C=1,
	%nl,write('La primera lista es sublista de la segunda').
mientras2([X|Y],[A|B],C,R):-
	%nl,write(X),nl,write(A),
	X\=A,
	append(R,[A],R1),
	copia(R1,R),
	mientras2([X|Y],B,C,R).
mientras2([],[A|B],C,R):-
	nl,write(A),
	mientras2([],B,C,R),
	nl,write(A),
	nl,write(R),
	append(R,[A],R1),
		nl,write(A),
	nl,write(R),
	nl,write(R1),
	copia(R1,R),
	nl, write(R).
mientras2([X|Y],[A|B],C,R):-
	X=A,
	mientras2(Y,B,C,R),
	C is 1.

eje9(L,X):-
	C is 0,
	invierte(L,L1),write(L1),
	sacalos(L1,X,C,M,N),
	write(M).
sacalos([],X1,0,[],N).
sacalos([X|Y],X1,C,M,N):-
	X\=X1,
	nl,write(X),write(' '),write(Y),write(' '),write(X1),write(' '),write(C),write(' '),write(M),write(' '),write(N),
	sacalos(Y,X1,C,M1,N1),
	append(M1,[X],N),
		nl,write(M1),write(' '),write(X),write(' '),write(N),

		nl, write(M1),
	copia(N,M).
copia([],[]).
copia([X|Y],M):-
	copia(Y,M1),
	append([X],M1,M).


sacalos([X|Y],X1,C,M,N):-
	X=X1,
	sacalos(Y,X1,C,M,N),
	C is 1.


eje18([],[]).
eje18([X|Y],L):-
	sacalos(Y,X,C,M,N),
	append([X],M,L).



























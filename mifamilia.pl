varon(gregorio).
varon(manuel).
varon(marcos).
varon(sabino).
varon(cornelio).
varon(daniel).
varon(felix).
varon(clemente).
varon(ambrosio).


mujer(eugenia).
mujer(andrea).
mujer(ascencia).
mujer(rufina).
mujer(edmunda).
mujer(patty).
mujer(cecilia).
mujer(asunta).
mujer(nicolasa).
mujer(lizett).
mujer(veronica).
mujer(valentina).

padres_de(manuel,eugenia,gregorio).
padres_de(marcos,eugenia,gregorio).
padres_de(andrea,eugenia,gregorio).
padres_de(gregorio,ascencia,sabino).
padres_de(eugenia,rufina,cornelio).
padres_de(edmunda,rufina,cornelio).
padres_de(patty,edmunda,daniel).
padres_de(lizett,cecilia,felix).
padres_de(veronica,cecilia,felix).
padres_de(felix,asunta,clemente).
padres_de(cecilia,nicolasa,ambrosio).
padres_de(valentina,lizett,manuel).

papa_de(gregorio,sabino).
papa_de(eugenia,cornelio).
papa_de(edmunda,cornelio).
papa_de(manuel,gregorio).
papa_de(marcos,gregorio).
papa_de(andrea,gregorio).
papa_de(lizett,felix).
papa_de(veronica,felix).
papa_de(felix,clemente).
papa_de(cecilia,ambrosio).
papa_de(valentina,manuel).
papa_de(patty,daniel).

mama_de(eugenia,rufina).
mama_de(edmunda,rufina).
mama_de(gregorio,ascencia).
mama_de(manuel,eugenia).
mama_de(marcos,eugenia).
mama_de(andrea,eugenia).
mama_de(lizett,cecilia).
mama_de(veronica,cecilia).
mama_de(cecilia,nicolasa).
mama_de(felix,asunta).
mama_de(valentina,lizett).
mama_de(patty,edmunda).

esposo_de(manuel,lizett).
esposo_de(gregorio,eugenia).
esposo_de(daniel,edmunda).
esposo_de(felix,cecilia).





hijo_de(X,Y):-
	varon(X),
	papa_de(X,Y),
		X\=Y.
hijo_de(X,Y):-
	varon(X),
	mama_de(X,Y),
		X\=Y.

hija_de(X,Y):-
	mujer(X),
	papa_de(X,Y),
		X\=Y.

hija_de(X,Y):-

	mujer(X),
	mama_de(X,Y),
	X\=Y.

hermano_de(X,Y):-
	varon(X),
	padres_de(X,M,P),
	padres_de(Y,M,P),
	X\=Y.
hermana_de(X,Y):-
	mujer(X),
	padres_de(X,M,P),
	padres_de(Y,M,P),
	X\=Y.




tio_de(X,Y):-
	varon(X),
	padres_de(Y,M,P),
	hermano_de(X,P),
	X\=Y.
tio_de(X,Y):-
	varon(X),
	padres_de(Y,M,P),
	hermano_de(X,M),
	X\=Y.

tio_de(X,Y):-
	varon(X),
	padres_de(Y,M,P),
	hermana_de(N,M),
	esposo_de(X,N),
	X\=Y.


tia_de(X,Y):-
	mujer(X),
	padres_de(Y,M,P),
	hermana_de(X,P),
	X\=Y.
tia_de(X,Y):-
	mujer(X),
	padres_de(Y,M,P),
	hermana_de(X,M),
	X\=Y.




sobrino_de(X,Y):-
	varon(X),
	padres_de(X,M,P),
	hermana_de(M,Y),
	X\=Y.
sobrino_de(X,Y):-
	varon(X),
	padres_de(X,M,P),
	hermano_de(P,Y),
	X\=Y.
sobrina_de(X,Y):-
	mujer(X),
	padres_de(X,M,P),
	hermana_de(M,Y),
	X\=Y.
sobrina_de(X,Y):-
	mujer(X),
	padres_de(X,M,P),
	hermano_de(P,Y),
	X\=Y.

nieto_de(X,Y):-
	varon(X),
	papa_de(X,P),
	papa_de(P,Y),
	X\=Y.
nieto_de(X,Y):-
	varon(X),
	mama_de(X,M),
	mama_de(M,Y),
	X\=Y.
nieto_de(X,Y):-
	varon(X),
	papa_de(X,P),
	mama_de(P,Y),
	X\=Y.
nieto_de(X,Y):-
	varon(X),
	mama_de(X,M),
	papa_de(M,Y),
	X\=Y.

nieta_de(X,Y):-
	mujer(X),
	papa_de(X,P),
	papa_de(P,Y),
	X\=Y.
nieta_de(X,Y):-
	mujer(X),
	mama_de(X,M),
	mama_de(M,Y),
	X\=Y.

nieta_de(X,Y):-
	mujer(X),
	papa_de(X,P),
	mama_de(P,Y),
	X\=Y.
nieta_de(X,Y):-
	mujer(X),
	mama_de(X,M),
	papa_de(M,Y),
	X\=Y.


bisabuelo_de(X,Y):-
	papa_de(A,X),
	papa_de(P,A),
	papa_de(Y,P),
	X\=Y.

bisabuelo_de(X,Y):-
	papa_de(A,X),
	mama_de(P,A),
	papa_de(Y,P),
	X\=Y.
bisabuelo_de(X,Y):-
	papa_de(A,X),
	papa_de(M,A),
	mama_de(Y,M),
	X\=Y.
bisabuelo_de(X,Y):-
	papa_de(A,X),
	mama_de(M,A),
	mama_de(Y,M),
	X\=Y.

bisabuela_de(X,Y):-
	mama_de(A,X),
	papa_de(P,A),
	papa_de(Y,P),
	X\=Y.

bisabuela_de(X,Y):-
	mama_de(A,X),
	mama_de(P,A),
	papa_de(Y,P),
	X\=Y.
bisabuela_de(X,Y):-
	mama_de(A,X),
	papa_de(M,A),
	mama_de(Y,M),
	X\=Y.
bisabuela_de(X,Y):-
	mama_de(A,X),
	mama_de(M,A),
	mama_de(Y,M),
	X\=Y.



bisnieto_de(X,Y):-
	varon(X),
	bisabuelo_de(Y,X),
	X\=Y.
bisnieto_de(X,Y):-
	varon(X),
	bisabuela_de(Y,X),
	X\=Y.

bisnieta_de(X,Y):-
	mujer(X),
	bisabuelo_de(Y,X),
	X\=Y.
bisnieta_de(X,Y):-
	mujer(X),
	bisabuela_de(Y,X),
	X\=Y.

primo_de(X,Y):-
	varon(X),
	papa_de(Y,P),
	tio_de(P,X),
	X\=Y.

primo_de(X,Y):-
	varon(X),
	mama_de(Y,P),
	tia_de(P,X),
	X\=Y.

prima_de(X,Y):-
	mujer(X),
	papa_de(Y,P),
	tio_de(P,X),
	X\=Y.

prima_de(X,Y):-
	mujer(X),
	mama_de(Y,P),
	tia_de(P,X),
	X\=Y.

cmp1([],[]).
cmp1([X|Y]):-
     write(X),
    cmp1(Y).
otro([],[],0).
otro(X,[X],1).
otro(X,[Y|Z],N):-
	X=Y,
	N=N+1,
	otro(X,Z,N).

menor(X,Y) :- X<Y.
menorig(X,Y) :- X=<Y.
%menor(X,Y) :- menor_alf(X,Y).
%menorig(X,Y) :- menorig_alf(X,Y).
%
%
menos_elem(X,[X|L],L).
menos_elem(X,[C|L],[C|LX]):-
	menos_elem(X,L,LX).
permutacion([],[]).
permutacion(L,[C|CL1]):-
	menos_elem(C,L,LC),
	permutacion(LC,CL1).


ordenada([]).
ordenada([X]).
ordenada([X,Y|L]) :- menorig(X,Y),ordenada([Y|L]).
orden_log(L1,L2) :- permutacion(L1,L2),ordenada(L2).





orden_bur(L,L) :- ordenada(L).

orden_bur(L,Lord) :-
         burbuja(L,L1),
	 orden_bur(L1,Lord).
burbuja([],[]).

burbuja([X],[X]).
burbuja([X,Y|L],Lburb) :-
               menorig(X,Y),
               burbuja([Y|L],L1),
               Lburb = [X|L1].
burbuja([X,Y|L],Lburb) :-
              menor(Y,X),
              burbuja([X|L],L1),
              Lburb = [Y|L1].

cmp2([],[]).
cmp2([E],[(E,1)]).
cmp2([C1,C2|RESTO],[(C1,1)|R]):-
	C1\=C2,
	cmp2([C2|RESTO],R).

cmp2([C1,C2|RESTO],[(C1,N2)|R]):-
	C1=C2,
	cmp2([C1,RESTO],[(C1,N)|R]),
	N2 is N+1.

tarea(L,L1):-
	orden_log(L,L2),
	cmp2(L2,L1).





















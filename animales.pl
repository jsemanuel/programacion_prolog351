como_es_el_animal('tiene pelos','El animal es un mamifero').
como_es_el_animal('da leche','El animal es un mamifero').

como_es_el_animal('tiene pelos o da leche','El animal es un mamifero').
como_es_el_animal('tiene pelos y da leche','El animal es un mamifero').

como_es_el_animal('tiene plumas','El animal es un ave').
como_es_el_animal('vuela','El animal es un ave').
como_es_el_animal('ave y vuela','El animal es un pajaro').
como_es_el_animal('pone huevos','El animal es un ave').
como_es_el_animal('tiene plumas o vuela y pone huevos','El animal es un ave').
como_es_el_animal('tiene plumas y vuela y pone huevos','El animal es un ave que es un pajaro').
como_es_el_animal('tiene plumas y vuela','El animal es un ave que es un pajaro').
como_es_el_animal('vuela y pone huevos','El animal es un ave que es un pajaro').

como_es_el_animal('come carne','El animal es un mamifero que es carnivoro').
como_es_el_animal('tiene garras','El animal es un mamifero que es carnivoro').
como_es_el_animal('tiene dientes punteagudos','El animal es un mamifero que es carnivoro').
como_es_el_animal('come carne o tiene dientes punteagudos','El animal es un mamifero que es carnivoro').
como_es_el_animal('come carne y tiene dientes punteagudos','El animal es un mamifero que es carnivoro').
como_es_el_animal('come carne o tiene dientes punteagudos y garras','El animal es un mamifero que es carnivoro').
como_es_el_animal('come carne y tiene dientes punteagudos y garras','El animal es un mamifero que es carnivoro').
como_es_el_animal('tiene pelos y come carne','El animal es un mamifero que es carnivoro').
como_es_el_animal('da leche y come carne','El animal es un mamifero que es carnivoro').
como_es_el_animal('tiene pelos y da leche y come carne','El animal es un mamifero que es carnivoro').

como_es_el_animal('tiene cascos','El animal es un mamifero que es un ungulado').
como_es_el_animal('rumia','El animal es un mamifero que es un ungulado').
como_es_el_animal('tiene cascos y rumia','El animal es un mamifero que es un ungulado').
como_es_el_animal('tiene cascos o rumia','El animal es un mamifero que es un ungulado').
como_es_el_animal('tiene pelos y rumia','El animal es un mamifero que es un ungulado').
como_es_el_animal('tiene pelos y da leche y tiene cascos y rumia','El animal es un mamifero que es un ungulado').
como_es_el_animal('tiene pelos y tiene cascos','El animal es un mamifero que es un ungulado').
como_es_el_animal('da leche y tiene cascos y rumia','El animal es un mamifero que es un ungulado').
como_es_el_animal('da leche y tiene cascos','El animal es un mamifero que es un ungulado').
como_es_el_animal('da leche y rumia','El animal es un mamifero que es un ungulado').

como_es_el_animal('come carne y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('tiene garras y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('tiene dientes punteagudos y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('come carne o tiene dientes punteagudos y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('come carne y tiene dientes punteagudos y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('come carne o tiene dientes punteagudos y garras y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('come carne y tiene dientes punteagudos y garras y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('tiene pelos y come carne y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('da leche y come carne y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').
como_es_el_animal('tiene pelos y da leche y come carne y color pardo','El animal es un mamifero que es carnivoro que puede ser un tigre o un guepardo').


como_es_el_animal('come carne y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene garras y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene dientes punteagudos y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne o tiene dientes punteagudos y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne y tiene dientes punteagudos y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne o tiene dientes punteagudos y garras y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne y tiene dientes punteagudos y garras y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene pelos y come carne y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('da leche y come carne y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene pelos y da leche y come carne y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').


como_es_el_animal('come carne y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene garras y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene dientes punteagudos y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne o tiene dientes punteagudos y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne y tiene dientes punteagudos y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne o tiene dientes punteagudos y garras y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne y tiene dientes punteagudos y garras y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene pelos y come carne y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('da leche y come carne y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene pelos y da leche y come carne y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').


como_es_el_animal('come carne y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene garras y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene dientes punteagudos y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne o tiene dientes punteagudos y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne y tiene dientes punteagudos y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne o tiene dientes punteagudos y garras y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('come carne y tiene dientes punteagudos y garras y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene pelos y come carne y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('da leche y come carne y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').
como_es_el_animal('tiene pelos y da leche y come carne y color pardo y tiene manchas negras','El animal es un mamifero carnivoro que es un guepardo').


como_es_el_animal('come carne y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene garras y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene dientes punteagudos y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne o tiene dientes punteagudos y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne y tiene dientes punteagudos y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne o tiene dientes punteagudos y garras y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('come carne y tiene dientes punteagudos y garras y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene pelos y come carne y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('da leche y come carne y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').
como_es_el_animal('tiene pelos y da leche y come carne y color pardo y tiene rayas negras','El animal es un mamifero carnivoro que es un tigre').


como_es_el_animal('no vuela','El animal puede ser un mamifero o un ave').

como_es_el_animal('tiene plumas y no vuela y tiene patas largas','El animal es un ave que puede ser una avestruz').

como_es_el_animal('tiene patas largas','El animal puede ser un ave que puede ser una avestruz').

como_es_el_animal('cuello largo','El animal puede ser un ave que puede ser una avestruz').




como_es_el_animal('tiene plumas y no vuela y tiene patas largas y cuello largo','El animal es un ave que puede ser una avestruz').

como_es_el_animal('tiene plumas y no vuela y tiene patas largas y cuello largo y es de color blanco y negro','El animal es un ave que es una avestruz').

como_es_el_animal('no vuela y tiene patas largas y cuello largo y es de color blanco y negro','El animal es un ave que es una avestruz').

como_es_el_animal('no vuela y tiene patas largas es de color blanco y negro','El animal es un ave que es una avestruz').

como_es_el_animal('no vuela y cuello largo y es de color blanco y negro','El animal es un ave que es una avestruz').

como_es_el_animal('no vuela y pone huevos y tiene patas largas y cuello largo y es de color blanco y negro','El animal es un ave que es una avestruz').

como_es_el_animal('tiene plumas y no vuela y es de color blanco y negro','El animal es un ave que puede ser un pinguino o una avestruz').

como_es_el_animal('tiene plumas y no vuela y pone huevos y es de color blanco y negro','El animal es un ave que puede ser un pinguino o una avestruz').
como_es_el_animal('no vuela y pone huevos y es de color blanco y negro','El animal es un ave que puede ser un pinguino o una avestruz').

como_es_el_animal('pone huevos y es de color blanco y negro','El animal es un ave que puede ser un pinguino o una avestruz').
como_es_el_animal('tiene plumas y es de color blanco y negro','El animal es un ave que puede ser un pinguino o una avestruz').

como_es_el_animal('pone huevos y no vuela y es de color blanco y negro','El animal es un ave que puede ser un pinguino o una avestruz').


como_es_el_animal('no vuela y nada','El animal es un ave que es un pinguino').
como_es_el_animal('no vuela y nada y es de color blanco y negro','El animal es un ave que es un pinguino').

como_es_el_animal('pone huevos y no vuela y nada','El animal es un ave que es un pinguino').
como_es_el_animal('pone huevos y nada','El animal es un ave que es un pinguino').
como_es_el_animal('tiene plumas y nada','El animal es un ave que es un pinguino').
como_es_el_animal('pone huevos y no vuela y nada y es de color blanco y negro','El animal es un ave que es un pinguino').
como_es_el_animal('pone huevos y nada y es de color blanco y negro','El animal es un ave que es un pinguino').

como_es_el_animal('tiene plumas y pone huevos y no vuela y nada','El animal es un ave que es un pinguino').
como_es_el_animal('tiene plumas y pone huevos y no vuela y nada y es de color blanco y negro','El animal es un ave que es un pinguino').

como_es_el_animal('tiene plumas y no vuela y nada','El animal es un ave que es un pinguino').
como_es_el_animal('tiene plumas y no vuela y nada y es de color blanco y negro','El animal es un ave que es un pinguino').

caracteristicas(X):-
	X='s',
	write('\n CARACTERISTICAS DEL ANIMAL'),
write('\n TIENE PELOS'),
write('\n DA LECHE'),
	write('\n TIENE PLUMAS'),
	write('\n VUELA'),
	write('\n PONE HUEVOS'),
write('\n COME CARNE'),
write('\n TIENE DIENTES PUNTEAGUDOS'),
write('\n TIENE GARRAS'),
	write('\n TIENE CASCOS'),
write('\n RUMIA'),
write('\n TIENE COLOR PARDO'),
write('\n TIENE MANCHAS NEGRAS'),
write('\n	TIENE RAYAS NEGRAS'),
write('\n NO VUELA'),
write('\n TIENE PATAS LARGAS),
write('\n TIENE CUELLO LARGO'),
write('\n COLOR BLANCO Y NEGRO'),
        write('\n NADA').



















varon(pedro).
varon(pedrito).
varon(hugo).
varon(rolando).
varon(ramiro).
varon(peter).
varon(percy).
varon(carlos).
varon(rolandojr).
varon(gonzalo).
varon(diego).
varon(matias).


mujer(luisa).
mujer(luisita).
mujer(mariarene).
mujer(estela).
mujer(norka).
mujer(sandra).
mujer(rosa).
mujer(jesica).
mujer(aleli).


padres_de(rolando,luisa,pedro).
padres_de(estela,luisita,pedrito).
padres_de(ramiro,luisita,pedrito).
padres_de(norka,mariarene,hugo).
padres_de(peter,mariarene,hugo).
padres_de(percy,mariarene,hugo).
padres_de(sandra,mariarene,hugo).
padres_de(carlos,mariarene,hugo).
padres_de(rosa,mariarene,hugo).
padres_de(aleli,norka,ramiro).
padres_de(gonzalo,norka,ramiro).
padres_de(diego,norka,ramiro).

padres_de(jesica,aleli,rolandojr).
padres_de(matias,aleli,rolandojr).

papa_de(rolando,pedro).
papa_de(rolandojr,rolando).
papa_de(estela,pedrito).
papa_de(ramiro,pedrito).
papa_de(jesica,rolandojr).
papa_de(matias,rolandojr).
papa_de(aleli,ramiro).
papa_de(gonzalo,ramiro).
papa_de(diego,ramiro).
papa_de(norka,hugo).
papa_de(peter,hugo).
papa_de(percy,hugo).
papa_de(sandra,hugo).
papa_de(carlos,hugo).
papa_de(rosa,hugo).

mama_de(rolando,luisa).
mama_de(jesica,aleli).
mama_de(matias,aleli).
mama_de(aleli,norka).
mama_de(gonzalo,norka).
mama_de(diego,norka).
mama_de(estela,luisita).
mama_de(ramiro,luisita).
mama_de(norka,mariarene).
mama_de(peter,mariarene).
mama_de(percy,mariarene).
mama_de(sandra,mariarene).
mama_de(carlos,mariarene).
mama_de(rosa,mariarene).

esposo_de(pedro,luisa).
esposo_de(pedrito,luisita).
esposo_de(ramiro,norka).
esposo_de(hugo,mariarene).
esposo_de(rolandojr,aleli).



hijo_de(X,Y):-
	varon(X),
	papa_de(X,Y),
		X\=Y.
hijo_de(X,Y):-
	varon(X),
	mama_de(X,Y),
		X\=Y.

hija_de(X,Y):-
	mujer(X),
	papa_de(X,Y),
		X\=Y.

hija_de(X,Y):-

	mujer(X),
	mama_de(X,Y),
	X\=Y.

hermano_de(X,Y):-
	varon(X),
	padres_de(X,M,P),
	padres_de(Y,M,P),
	X\=Y.
hermana_de(X,Y):-
	mujer(X),
	padres_de(X,M,P),
	padres_de(Y,M,P),
	X\=Y.




tio_de(X,Y):-
	varon(X),
	padres_de(Y,M,P),
	hermano_de(X,P),
	X\=Y.
tio_de(X,Y):-
	varon(X),
	padres_de(Y,M,P),
	hermano_de(X,M),
	X\=Y.

tio_de(X,Y):-
	varon(X),
	padres_de(Y,M,P),
	hermana_de(N,M),
	esposo_de(X,N),
	X\=Y.


tia_de(X,Y):-
	mujer(X),
	padres_de(Y,M,P),
	hermana_de(X,P),
	X\=Y.
tia_de(X,Y):-
	mujer(X),
	padres_de(Y,M,P),
	hermana_de(X,M),
	X\=Y.




sobrino_de(X,Y):-
	varon(X),
	padres_de(X,M,P),
	hermana_de(M,Y),
	X\=Y.
sobrino_de(X,Y):-
	varon(X),
	padres_de(X,M,P),
	hermano_de(P,Y),
	X\=Y.
sobrina_de(X,Y):-
	mujer(X),
	padres_de(X,M,P),
	hermana_de(M,Y),
	X\=Y.
sobrina_de(X,Y):-
	mujer(X),
	padres_de(X,M,P),
	hermano_de(P,Y),
	X\=Y.

nieto_de(X,Y):-
	varon(X),
	papa_de(X,P),
	papa_de(P,Y),
	X\=Y.
nieto_de(X,Y):-
	varon(X),
	mama_de(X,M),
	mama_de(M,Y),
	X\=Y.
nieto_de(X,Y):-
	varon(X),
	papa_de(X,P),
	mama_de(P,Y),
	X\=Y.
nieto_de(X,Y):-
	varon(X),
	mama_de(X,M),
	papa_de(M,Y),
	X\=Y.

nieta_de(X,Y):-
	mujer(X),
	papa_de(X,P),
	papa_de(P,Y),
	X\=Y.
nieta_de(X,Y):-
	mujer(X),
	mama_de(X,M),
	mama_de(M,Y),
	X\=Y.

nieta_de(X,Y):-
	mujer(X),
	papa_de(X,P),
	mama_de(P,Y),
	X\=Y.
nieta_de(X,Y):-
	mujer(X),
	mama_de(X,M),
	papa_de(M,Y),
	X\=Y.


bisabuelo_de(X,Y):-
	papa_de(A,X),
	papa_de(P,A),
	papa_de(Y,P),
	X\=Y.

bisabuelo_de(X,Y):-
	papa_de(A,X),
	mama_de(P,A),
	papa_de(Y,P),
	X\=Y.
bisabuelo_de(X,Y):-
	papa_de(A,X),
	papa_de(M,A),
	mama_de(Y,M),
	X\=Y.
bisabuelo_de(X,Y):-
	papa_de(A,X),
	mama_de(M,A),
	mama_de(Y,M),
	X\=Y.

bisabuela_de(X,Y):-
	mama_de(A,X),
	papa_de(P,A),
	papa_de(Y,P),
	X\=Y.

bisabuela_de(X,Y):-
	mama_de(A,X),
	mama_de(P,A),
	papa_de(Y,P),
	X\=Y.
bisabuela_de(X,Y):-
	mama_de(A,X),
	papa_de(M,A),
	mama_de(Y,M),
	X\=Y.
bisabuela_de(X,Y):-
	mama_de(A,X),
	mama_de(M,A),
	mama_de(Y,M),
	X\=Y.



bisnieto_de(X,Y):-
	varon(X),
	bisabuelo_de(Y,X),
	X\=Y.
bisnieto_de(X,Y):-
	varon(X),
	bisabuela_de(Y,X),
	X\=Y.

bisnieta_de(X,Y):-
	mujer(X),
	bisabuelo_de(Y,X),
	X\=Y.
bisnieta_de(X,Y):-
	mujer(X),
	bisabuela_de(Y,X),
	X\=Y.

primo_de(X,Y):-
	varon(X),
	papa_de(Y,P),
	tio_de(P,X),
	X\=Y.

primo_de(X,Y):-
	varon(X),
	mama_de(Y,P),
	tia_de(P,X),
	X\=Y.

prima_de(X,Y):-
	mujer(X),
	papa_de(Y,P),
	tio_de(P,X),
	X\=Y.

prima_de(X,Y):-
	mujer(X),
	mama_de(Y,P),
	tia_de(P,X),
	X\=Y.
abuelo_de(X,Y):-
	varon(X),
	papa_de(Y,P),
	papa_de(P,X),
	X\=Y.
abuela_de(X,Y):-
	mujer(X),
	mama_de(Y,P),
	mama_de(P,X),
	X\=Y.

abuelo_de(X,Y):-
	varon(X),
	mama_de(Y,M),
	papa_de(M,X),
	X\=Y.
abuela_de(X,Y):-
	mujer(X),
	papa_de(Y,P),
	mama_de(P,X),
	X\=Y.
yerno_de(X,Y):-
	varon(Y),
	esposo_de(Y,E),
	papa_de(E,X),
	X\=Y.
nuera_de(X,Y):-
	mujer(Y),
	esposo_de(E,Y),
	mama_de(E,X),
	X\=Y.
nuera_de(X,Y):-
	mujer(Y),
	esposo_de(E,Y),
	papa_de(E,X),
	X\=Y.

yerno_de(X,Y):-
	varon(Y),
	esposo_de(Y,E),
	mama_de(E,X),
	X\=Y.

